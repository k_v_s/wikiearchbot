# Wikipedia Search Bot

Бот для получения информации из wikipedia.org с открытым исходным кодом. Для портфолио.

## Возможности и будущее бота

***Реализовано:***<br/>
✅Получение информации с помощью wikipedia.org<br/>
✅Добавление пользователей в базу данных SQLite <br/>
***Будет реализовано в будущем:***<br/>
❎Наличие административной панели для управления ботом<br/>
❎Замена SQLite на MongoDB<br/>
❎Возможность осуществления рассылки для своих пользователей<br/>
❎Возможность получения статистики активности пользователей<br/>


## Установка и использование

Для использования данного Telegram-бота нужен сервер на базе Linux Debian 10 с белым(статическим) IP-адресом.

***Установка всех необходимых зависимостей и клонирование репозитория***

```
sudo apt-get install git python3 python3-pip openssl systemd nano
git clone https://gitlab.com/k_v_s/wikiearchbot.git
cd wikisearchbot/
sudo pip3 install -r requirements.txt
```
***Конфигурация бота***

Создание базы данных для хранения данных о пользователях:
```
python3 make_db.py
```
Создание приватного ключа и самоподписного сертификата: 
```
openssl genrsa -out webhook_pkey.pem 2048
openssl req -new -x509 -days 3650 -key webhook_pkey.pem -out webhook_cert.pem
```
Заходим в конфиг бота:
```
nano config.py
```
Токен для работы Telegram-бота(нужно получить в https://t.me/BotFather):
```
token = 'ТОКЕН БОТА'
```

Заполняем данные для работы серверной части бота:
```
WEBHOOK_HOST = 'IP-адрес сервера, на котором запускается бот'
WEBHOOK_PORT = 443  # 443, 80, 88 или 8443 (порт должен быть открыт!)
WEBHOOK_LISTEN = '0.0.0.0'  # На некоторых серверах придется указывать такой же IP, что и выше

WEBHOOK_SSL_CERT = './webhook_cert.pem'  # Путь к сертификату
WEBHOOK_SSL_PRIV = './webhook_pkey.pem'  # Путь к приватному ключу

WEBHOOK_URL_BASE = "https://%s:%s" % (WEBHOOK_HOST, WEBHOOK_PORT)
WEBHOOK_URL_PATH = "/%s/" % (token)
```
Зажимаем Ctrl+X, далее - Y, далее - Enter(сохранили изменения)

***Запуск бота***

Настройка systemd:
```
cd ..
mv wikisearchbot/ /usr/local/bin
nano bot.service 
```
Далее копируем и вставляем:
```
[Unit]
Description=Telegram bot 'Wikipedia Search Bot'
After=syslog.target
After=network.target

[Service]
Type=simple
User=root
WorkingDirectory=/usr/local/bin/wikisearchbot
ExecStart=/usr/bin/python3 /usr/local/bin/wikisearchbot/main.py
RestartSec=10
Restart=always
 
[Install]
WantedBy=multi-user.target
```
Зажимаем Ctrl+X, далее - Y, далее - Enter(сохранили изменения) и переносим файл в нужную папку:
```
mv bot.service /etc/systemd/system
```

Запускаем бота:
```
systemctl daemon-reload
systemctl enable bot
systemctl start bot
systemctl status bot

```
Готово!

## Об авторе

Автор: @k_v_s <br/>
Telegram: https://t.me/k_v_s_03 <br/>
Mail: kvs_2022@bk.ru <br/>

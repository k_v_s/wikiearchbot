import telebot
from config import *
import wikipedia
import cherrypy
from db import *


bot = telebot.TeleBot(token)
wikipedia.set_lang("ru")


@bot.message_handler(commands=['start', 'help'])
def start(message):
    bot.reply_to(message, 'Привет! Меня зовут Wiki, я помогу тебе найти информацию из Wikipedia. Что нужно узнать?(например, Python)')
    user = (message.chat.id, message.from_user.first_name, message.from_user.last_name, message.from_user.username)
    add_user(user)


@bot.message_handler(content_types=['text'])
def text_handler(message):
    try:
        wait_msg = bot.reply_to(message, '***Ищу информацию***', parse_mode='markdown')
        summary = wikipedia.summary(message.text, sentences = 7)
        search_list = wikipedia.search(message.text)
        search = ', '.join(search_list)

        answer = '***Вот, что я нашел по твоему запросу: ***' + '\n' + '\n' + summary + '\n' + '\n' + '***Похожие запросы: ***' + search
        bot.edit_message_text(chat_id=message.chat.id, message_id=wait_msg.id, text=answer, parse_mode='markdown')
        #bot.reply_to(message, answer, parse_mode='markdown')

    except Exception as e:
        print(e)
        bot.reply_to(message, 'Произошла ошибка! У меня не получилось найти информацию. Скорее всего, в Wikipedia нет такой информации.')


class WebhookServer(object):
    @cherrypy.expose
    def index(self):
        if 'content-length' in cherrypy.request.headers and \
                        'content-type' in cherrypy.request.headers and \
                        cherrypy.request.headers['content-type'] == 'application/json':
            length = int(cherrypy.request.headers['content-length'])
            json_string = cherrypy.request.body.read(length).decode("utf-8")
            update = telebot.types.Update.de_json(json_string)
            # Эта функция обеспечивает проверку входящего сообщения
            bot.process_new_updates([update])
            return ''
        else:
            raise cherrypy.HTTPError(403)


bot.remove_webhook()

bot.set_webhook(url=WEBHOOK_URL_BASE + WEBHOOK_URL_PATH,
                certificate=open(WEBHOOK_SSL_CERT, 'r'))


# Указываем настройки сервера CherryPy
cherrypy.config.update({
    'server.socket_host': WEBHOOK_LISTEN,
    'server.socket_port': WEBHOOK_PORT,
    'server.ssl_module': 'builtin',
    'server.ssl_certificate': WEBHOOK_SSL_CERT,
    'server.ssl_private_key': WEBHOOK_SSL_PRIV
})

 # Собственно, запуск!
cherrypy.quickstart(WebhookServer(), WEBHOOK_URL_PATH, {'/': {}})


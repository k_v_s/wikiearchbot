import sqlite3

conn = sqlite3.connect('users.db')

cur = conn.cursor()

cur.execute("""CREATE TABLE IF NOT EXISTS users(user_id INT UNIQUE, fname TEXT, lname TEXT, username TEXT);""")
cur.execute("""CREATE UNIQUE INDEX user_idx ON users(user_id);""")
conn.commit()
conn.close()